# DKRZ User Workshop: using the DKRZ tape archive

2022-10-13, 2 p.m. - 3 p.m.

Daniel Heydebreck, Andrej Fast, Hadieh Monajemi

**Note:** We will use "*tape archive*", "*HSM*" and "*StrongLink*" interchangably. Technically, this is not correct.

## Useful links

* HSM/StrongLink documentation: https://docs.dkrz.de/doc/datastorage/hsm/index.html
* pyslk documentation (slk Python wrapper): https://hsm-tools.gitlab-pages.dkrz.de/pyslk/index.html
* packems: https://code.mpimet.mpg.de/projects/esmenv/wiki/Packems
* How to run `slk retrieve` interactively: https://docs.dkrz.de/doc/datastorage/hsm/retrievals.html#interactive-retrieval


## Preparation

* location of training data on the  (please copy):
  * Levante / Lustre: `/work/bm0146/k204221/material_hsm_workshop`
  * HSM: `/workshop/material_hsm_workshop`
* training area in the HSM/StrongLink: `/workshop`
* please load the module `slk/3.3.64-gcc-11.2.0`
* please login via `slk login` using your DKRZ login credentials
* please create a user folder manually: `slk_helpers mkdir /workshop/$USER`

## Hands-On Part I

### local modules

```bash
module load slk/3.3.64-gcc-11.2.0
module load netcdf-c
```

We need ncdump later on and, hence, load `netcdf-c`.


### Archive a file, check it and provide it to others

Change into directory with one standardized file and have a look into it

```bash
$ cd material_hsm_workshop/data/mixed_files/
$ ls -lah
total 84K
drwxrwx--- 1 root vboxsf 4,0K Okt  8 00:25 .
drwxrwx--- 1 root vboxsf 4,0K Okt  8 00:25 ..
-rwxrwx--- 1 root vboxsf  27K Feb 23  2021 ACDD_2005092200_sst_21-24.en.nc
-rwxrwx--- 1 root vboxsf 1,6K Okt  8 00:23 test_netcdf_a01.nc
...
$ ncdump -h ACDD_2005092200_sst_21-24.en.nc | tail -n 30
```

Archive this file

```bash
$ slk archive ACDD_2005092200_sst_21-24.en.nc /workshop/$USER/mixed_files
# target folders are automatically created
```

We see no command line output similar to `cp`. Information on this archival process, but also on errors and called slk commands can be found in the slk log file: `~/.slk/slk-cli.log`.

If you want to see the progress of an archival process, please run `slk archive` with `-v`:

```bash
$ slk archive *01.nc -v /workshop/k204221/mixed_files
[========================================|] 100% complete. Files archived: 5/5, [10.8K/10.8K].
```

In order to get more detailed information please use `-vv`:

```bash
$ slk archive *.nc -vv /workshop/k204221/mixed_files
test_netcdf_a01.nc SKIPPED
test_netcdf_b01.nc SKIPPED
test_netcdf_c01.nc SKIPPED
test_netcdf_d01.nc SKIPPED
test_netcdf_header01.nc SKIPPED
test_netcdf_a02.nc SUCCESSFUL
test_netcdf_c02.nc SUCCESSFUL
ACDD_2005092200_sst_21-24.en.nc SKIPPED
test_netcdf_b02.nc SUCCESSFUL
test_netcdf_d02.nc SUCCESSFUL
test_netcdf_header02.nc SUCCESSFUL
```

List the file. Check the file status and the imported metadata.

```bash
$ slk list /workshop/$USER/mixed_files
-rwxrwx---- k204221     bm0146         26.6K   23 Feb 2021 08:27 ACDD_2005092200_sst_21-24.en.nc
-rwxrwx---- k204221     bm0146          1.5K   08 Oct 2022 00:23 test_netcdf_a01.nc
-rwxrwx---- k204221     bm0146          1.5K   08 Oct 2022 00:23 test_netcdf_a02.nc
-rwxrwx---- k204221     bm0146          1.6K   08 Oct 2022 00:23 test_netcdf_b01.nc
-rwxrwx---- k204221     bm0146          1.6K   08 Oct 2022 00:23 test_netcdf_b02.nc
-rwxrwx---- k204221     bm0146          1020   08 Oct 2022 00:23 test_netcdf_c01.nc
-rwxrwx---- k204221     bm0146          1020   08 Oct 2022 00:23 test_netcdf_c02.nc
-rwxrwx---- k204221     bm0146           888   08 Oct 2022 00:23 test_netcdf_d01.nc
-rwxrwx---- k204221     bm0146           888   08 Oct 2022 00:23 test_netcdf_d02.nc
-rwxrwx---- k204221     bm0146          5.8K   08 Oct 2022 00:23 test_netcdf_header01.nc
-rwxrwx---- k204221     bm0146          5.8K   08 Oct 2022 00:23 test_netcdf_header02.nc
Files: 11

# calculate checksum locally
$ sha512sum ACDD_2005092200_sst_21-24.en.nc
9f4f119db18aa71770d830ce301e7d1fca1393fe10f6fe8b44ef38ca880d36978f4c51c62b5b402e805c7172839f02e5fc68162b74082e055a8a0a3d8eb675cf  ACDD_2005092200_sst_21-24.en.nc

# get checksum from StrongLink (might take several minutes until checksums of freshly archived files are calculated)
$ slk_helpers checksum /workshop/$USER/mixed_files/ACDD_2005092200_sst_21-24.en.nc
adler32: 8840048a
sha512: 9f4f119db18aa71770d830ce301e7d1fca1393fe10f6fe8b44ef38ca880d36978f4c51c62b5b402e805c7172839f02e5fc68162b74082e055a8a0a3d8eb675cf

```


### Retrieve data

Get the data back (retrieval)

```bash
# change into a new directory for retrievals
$ cd ../../..
$ mkdir -p retrievals/test01 retrievals/test02 retrievals/test03
$ cd retrievals/test01

# basic retrieval
$ slk retrieve /workshop/$USER/mixed_files/ACDD_2005092200_sst_21-24.en.nc .
[========================================/] 100% complete. Files retrieved: 1/1, [26.6K/26.6K].
$ ls -la
total 36
drwxr-xr-x. 2 k204221 bm0146  4096 Okt  6 12:43 .
drwxr-xr-x. 6 k204221 bm0146  4096 Okt  6 12:23 ..
-rw-r--r--. 1 k204221 bm0146 27204 Okt  6 12:43 ACDD_2005092200_sst_21-24.en.nc

# retrieve the file a second time
$ slk retrieve /workshop/$USER/mixed_files/ACDD_2005092200_sst_21-24.en.nc .
[========================================/] 100% complete. Files retrieved: 1/1, [26.6K/26.6K].

$ ls -la
total 40
drwxr-xr-x. 2 k204221 bm0146  4096 Okt  6 12:42 .
drwxr-xr-x. 6 k204221 bm0146  4096 Okt  6 12:23 ..
-rw-r--r--. 1 k204221 bm0146 27204 Okt  6 12:42 ACDD_2005092200_sst_21-24.en.DUPLICATE_FILENAME.60981176011.4.nc
-rw-r--r--. 1 k204221 bm0146 27204 Okt  6 12:23 ACDD_2005092200_sst_21-24.en.nc

# just skip the existing file:
$ rm ACDD_2005092200_sst_21-24.en.DUPLICATE_FILENAME.60981176011.4.nc
$ slk retrieve -s /workshop/$USER/mixed_files/ACDD_2005092200_sst_21-24.en.nc .
[========================================|] 100% complete. Files retrieved: 0/1, [0B/26.6K]. Files skipped: 1.
$ echo $?
130
$ ls -la
total 36
drwxr-xr-x. 2 k204221 bm0146  4096 Okt  6 12:43 .
drwxr-xr-x. 6 k204221 bm0146  4096 Okt  6 12:23 ..
-rw-r--r--. 1 k204221 bm0146 27204 Okt  6 12:43 ACDD_2005092200_sst_21-24.en.nc

# overwrite target file
$ slk retrieve -f /workshop/$USER/mixed_files/ACDD_2005092200_sst_21-24.en.nc .
[========================================/] 100% complete. Files retrieved: 1/1, [26.6K/26.6K].
$ ls -la
total 36
drwxr-xr-x. 2 k204221 bm0146  4096 Okt  6 12:46 .
drwxr-xr-x. 6 k204221 bm0146  4096 Okt  6 12:23 ..
-rw-r--r--. 1 k204221 bm0146 27204 Okt  6 12:46 ACDD_2005092200_sst_21-24.en.nc
```

Questions?!

### Advanced retrieval

Do some more archival. We have prepared some multi-year data: one file per year, 12 years.

```bash
$ cd ../../material_hsm_workshop/data/
$ ls -la ocean_temperature/
total 7960
drwxr-xr-x. 2 k204221 bm0146   4096 Okt  6 12:02 .
drwxr-xr-x. 6 k204221 bm0146   4096 Okt  6 12:23 ..
-rw-r--r--. 1 k204221 bm0146 679311 Okt  6 12:02 surface_iow_day3d_temp_emep_2003.nc
-rw-r--r--. 1 k204221 bm0146 676876 Okt  6 12:02 surface_iow_day3d_temp_emep_2004.nc
...
-rw-r--r--. 1 k204221 bm0146 673021 Okt  6 12:02 surface_iow_day3d_temp_emep_2014.nc

$ slk archive ocean_temperature/*.nc /workshop/$USER/ocean -vv
/scratch/k/k204221/material_hsm_workshop/data/ocean_temperature/surface_iow_day3d_temp_emep_2003.nc SUCCESSFUL
/scratch/k/k204221/material_hsm_workshop/data/ocean_temperature/surface_iow_day3d_temp_emep_2004.nc SUCCESSFUL
...
/scratch/k/k204221/material_hsm_workshop/data/ocean_temperature/surface_iow_day3d_temp_emep_2014.nc SUCCESSFUL
```

Find and retrieve only model output from 2010 and onwards => need `slk search`

```bash
# generate a JSON search query (using regular expressions; no wildcards/globs)
$ slk_helpers gen_file_query /workshop/$USER/ocean/surface_iow_day3d_temp_emep_201..nc
{"$and":[{"path":{"$gte":"/workshop/k204221/ocean","$max_depth":1}},{"resources.name":{"$regex":"surface_iow_day3d_temp_emep_201..nc"}}]}

$ slk search '{"$and":[{"path":{"$gte":"/workshop/k204221/ocean","$max_depth":1}},{"resources.name":{"$regex":"surface_iow_day3d_temp_emep_201..nc"}}]}'
Search continuing.. ....
Search ID: 204135

$ slk list 204135
-rw-r--r--- k204221     bm0146        660.9K   06 Oct 2022 01:02 /workshop/k204221/ocean/surface_iow_day3d_temp_emep_2012.nc
-rw-r--r--- k204221     bm0146        657.2K   06 Oct 2022 01:02 /workshop/k204221/ocean/surface_iow_day3d_temp_emep_2014.nc
-rw-r--r--- k204221     bm0146        665.2K   06 Oct 2022 01:02 /workshop/k204221/ocean/surface_iow_day3d_temp_emep_2010.nc
-rw-r--r--- k204221     bm0146        662.8K   06 Oct 2022 01:02 /workshop/k204221/ocean/surface_iow_day3d_temp_emep_2011.nc
-rw-r--r--- k204221     bm0146        660.6K   06 Oct 2022 01:02 /workshop/k204221/ocean/surface_iow_day3d_temp_emep_2013.nc
Files: 5

$ cd ../../../retrievals

# plain retrieval into target directory
$ slk retrieve 204135 test02/
[========================================-] 100% complete. Files retrieved: 5/5, [3.2M/3.2M].

$ ls -la test02total 3340
drwxr-xr-x. 2 k204221 bm0146   4096 Okt  6 13:03 .
drwxr-xr-x. 7 k204221 bm0146  16384 Okt  6 13:03 ..
-rw-r--r--. 1 k204221 bm0146 681188 Okt  6 13:03 surface_iow_day3d_temp_emep_2010.nc
-rw-r--r--. 1 k204221 bm0146 678709 Okt  6 13:03 surface_iow_day3d_temp_emep_2011.nc
-rw-r--r--. 1 k204221 bm0146 676795 Okt  6 13:03 surface_iow_day3d_temp_emep_2012.nc
-rw-r--r--. 1 k204221 bm0146 676456 Okt  6 13:03 surface_iow_day3d_temp_emep_2013.nc
-rw-r--r--. 1 k204221 bm0146 673021 Okt  6 13:03 surface_iow_day3d_temp_emep_2014.nc

# reconstruct folder structure locally -- useful if several files have the same name
$ slk retrieve 204135 test03/ -ns
[========================================-] 100% complete. Files retrieved: 5/5, [3.2M/3.2M].

$ ls test03
workshop

$ ls -la retrievals/workshop/k204221/ocean/
total 3328
drwxr-xr-x. 2 k204221 bm0146   4096 Okt  6 02:05 .
drwxr-xr-x. 3 k204221 bm0146   4096 Okt  6 02:05 ..
-rw-r--r--. 1 k204221 bm0146 681188 Okt  6 02:05 surface_iow_day3d_temp_emep_2010.nc
-rw-r--r--. 1 k204221 bm0146 678709 Okt  6 02:05 surface_iow_day3d_temp_emep_2011.nc
-rw-r--r--. 1 k204221 bm0146 676795 Okt  6 02:05 surface_iow_day3d_temp_emep_2012.nc
-rw-r--r--. 1 k204221 bm0146 676456 Okt  6 02:05 surface_iow_day3d_temp_emep_2013.nc
-rw-r--r--. 1 k204221 bm0146 673021 Okt  6 02:05 surface_iow_day3d_temp_emep_2014.nc
```

## Hands-On Part II

### useful links:

* https://docs.dkrz.de/doc/datastorage/hsm/ref_metadata.html#schema-netcdf
* https://docs.dkrz.de/doc/datastorage/hsm/ref_metadata.html#schema-netcdf-header


### Archive data (if not done in hands-on part I)

We archive two multi-year data sets. The first one was already used in the first part of the hands-on sessions. The difference between both data sets is:

* `ocean_temperature`: all files in one folder; annual files; the `year` of each file is in the filename
* `ocean_temperature_same_name`: each file is in a subfolder; each subfolder has the name of the `year`; all files have the same name


```bash
$ slk archive material_hsm_workshop/data/ocean_temperature/*.nc /workshop/$USER/ocean -vv
/scratch/k/k204221/material_hsm_workshop/data/ocean_temperature/surface_iow_day3d_temp_emep_2003.nc SUCCESSFUL
/scratch/k/k204221/material_hsm_workshop/data/ocean_temperature/surface_iow_day3d_temp_emep_2004.nc SUCCESSFUL
...
/scratch/k/k204221/material_hsm_workshop/data/ocean_temperature/surface_iow_day3d_temp_emep_2014.nc SUCCESSFUL
```

and

```bash
$ slk archive data/ocean_temperature_same_name/* /workshop/$USER/ocean_sn -vv
/scratch/k/k204221/material_hsm_workshop/data/ocean_temperature_same_name/2003/surface_iow_day3d_temp_emep.nc SUCCESSFUL
/scratch/k/k204221/material_hsm_workshop/data/ocean_temperature_same_name/2004/surface_iow_day3d_temp_emep.nc SUCCESSFUL
...
/scratch/k/k204221/material_hsm_workshop/data/ocean_temperature_same_name/2014/surface_iow_day3d_temp_emep.nc SUCCESSFUL
```

### Retrieve data from 2010 and onwards

We want to list and retrieve all files for the years 2010 and onwards. During the file archival, the first and last values of the `time` variable are taken from the file and written into the metadata fields `netcdf.Time_Min` and `netcdf.Time_Max`. We can search data based on the values in these fields:

```bash


$ slk search '{"$and": [{"path": {"$gte": "/workshop/k204221/ocean"}}, {"netcdf.Time_Min": {"$gte": "2010-01-01"}}]}'
Search continuing. .....
Search ID: 205478
$ slk list 205478
-rwxrwx---- k204221     bm0146        676.3K   07 Oct 2022 12:50 /workshop/k204221/ocean/surface_iow_day3d_temp_emep_2010.nc
-rwxrwx---- k204221     bm0146        672.0K   07 Oct 2022 12:50 /workshop/k204221/ocean/surface_iow_day3d_temp_emep_2012.nc
-rwxrwx---- k204221     bm0146        668.3K   07 Oct 2022 12:50 /workshop/k204221/ocean/surface_iow_day3d_temp_emep_2014.nc
-rwxrwx---- k204221     bm0146        673.9K   07 Oct 2022 12:50 /workshop/k204221/ocean/surface_iow_day3d_temp_emep_2011.nc
-rwxrwx---- k204221     bm0146        671.7K   07 Oct 2022 12:50 /workshop/k204221/ocean/surface_iow_day3d_temp_emep_2013.nc
Files: 5
```

Now, we retrieve the files.

```bash
$ slk retrieve 205478 test04/
[========================================|] 100% complete. Files retrieved: 5/5, [3.3M/3.3M].
$ ls -la test04
total 3384
drwxrwx--- 1 root vboxsf   4096 Okt  8 01:37  .
drwxrwx--- 1 root vboxsf   4096 Okt  8 01:36  ..
-rwxrwx--- 1 root vboxsf 692542 Okt  8 01:37 surface_iow_day3d_temp_emep_2010.nc
-rwxrwx--- 1 root vboxsf 690063 Okt  8 01:37 surface_iow_day3d_temp_emep_2011.nc
-rwxrwx--- 1 root vboxsf 688149 Okt  8 01:37 surface_iow_day3d_temp_emep_2012.nc
-rwxrwx--- 1 root vboxsf 687810 Okt  8 01:37 surface_iow_day3d_temp_emep_2013.nc
-rwxrwx--- 1 root vboxsf 684375 Okt  8 01:37 surface_iow_day3d_temp_emep_2014.nc
```

### Search and retrieve several files with the same name

Now we search for the same files in the second dataset and retrieve the files


```bash
$ slk search '{"$and": [{"path": {"$gte": "/workshop/k204221/ocean_sn"}}, {"netcdf.Time_Min": {"$gte": "2010-01-01"}}]}'
Search continuing. .....
Search ID: 205478
$ slk list 205478
-rwxrwx---- k204221     bm0146        676.3K   08 Oct 2022 01:33 /workshop/k204221/ocean_sn/2010/surface_iow_day3d_temp_emep.nc
-rwxrwx---- k204221     bm0146        672.0K   08 Oct 2022 01:33 /workshop/k204221/ocean_sn/2012/surface_iow_day3d_temp_emep.nc
-rwxrwx---- k204221     bm0146        668.3K   08 Oct 2022 01:33 /workshop/k204221/ocean_sn/2014/surface_iow_day3d_temp_emep.nc
-rwxrwx---- k204221     bm0146        671.7K   08 Oct 2022 01:33 /workshop/k204221/ocean_sn/2013/surface_iow_day3d_temp_emep.nc
-rwxrwx---- k204221     bm0146        673.9K   08 Oct 2022 01:33 /workshop/k204221/ocean_sn/2011/surface_iow_day3d_temp_emep.nc
Files: 5
```

First try to retrieve the data:

```bash
$ slk retrieve 205539 test05
[========================================|] 100% complete. Files retrieved: 5/5, [3.3M/3.3M].

$ ls test05 -la
total 3384
drwxr-xr-x.  2 k204221 bm0146   4096 Okt  8 02:18 .
drwxr-xr-x. 18 k204221 bm0146   4096 Okt  8 02:18 ..
-rw-------.  1 k204221 bm0146 684375 Okt  8 02:18 surface_iow_day3d_temp_emep.DUPLICATE_FILENAME.60996734023.1.nc
-rw-------.  1 k204221 bm0146 690063 Okt  8 02:18 surface_iow_day3d_temp_emep.DUPLICATE_FILENAME.60996743010.1.nc
-rw-------.  1 k204221 bm0146 688149 Okt  8 02:18 surface_iow_day3d_temp_emep.DUPLICATE_FILENAME.60996744010.1.nc
-rw-------.  1 k204221 bm0146 687810 Okt  8 02:18 surface_iow_day3d_temp_emep.DUPLICATE_FILENAME.60996745010.1.nc
-rw-------.  1 k204221 bm0146 692542 Okt  8 02:18 surface_iow_day3d_temp_emep.nc
```

The files overwrite themselves and we do not know which file is which. Instead, we can use the paramter `-ns` to reconstruct the directory tree.

```
$ slk retrieve -ns 205539 test06
[========================================|] 100% complete. Files retrieved: 5/5, [3.3M/3.3M].

$ ls -laR test06
# [schnipp]
test06/workshop/k204221/ocean_sn/2010:
-rw-r--r--. 1 k204221 bm0146 692542 Okt  8 02:20 surface_iow_day3d_temp_emep.nc

test06/workshop/k204221/ocean_sn/2011:
-rw-r--r--. 1 k204221 bm0146 690063 Okt  8 02:20 surface_iow_day3d_temp_emep.nc

test06/workshop/k204221/ocean_sn/2012:
-rw-r--r--. 1 k204221 bm0146 688149 Okt  8 02:20 surface_iow_day3d_temp_emep.nc

test06/workshop/k204221/ocean_sn/2013:
-rw-r--r--. 1 k204221 bm0146 687810 Okt  8 02:20 surface_iow_day3d_temp_emep.nc

test06/workshop/k204221/ocean_sn/2014:
-rw-r--r--. 1 k204221 bm0146 684375 Okt  8 02:20 surface_iow_day3d_temp_emep.nc
```

### Set a few metadata fields.

We have a few files which are lacking metadata.

```bash
$ slk list /workshop/k204221/mixed_files/*no_attributes*
-rwxrwx---- k204221     bm0146         23.6K   08 Oct 2022 02:26 test_netcdf_no_attributes_01.nc
-rwxrwx---- k204221     bm0146         23.6K   08 Oct 2022 02:26 test_netcdf_no_attributes_02.nc
-rwxrwx---- k204221     bm0146         23.6K   08 Oct 2022 02:26 test_netcdf_no_attributes_03.nc
Files: 3

$ slk_helpers metadata /workshop/k204221/mixed_files/test_netcdf_no_attributes_01.nc
netcdf
  Var_Long_Name: zeit,latitude,height,mass_concentration_of_nitric_acid_in_air,longitude
  Var_Std_Name: zeit,latitude,height,longitude
  Title: dummy_value
  Data:
  Var_Name: zeit,lat,z,data_var,lon
netcdf_header
  Var_Long_Name: zeit,latitude,height,mass_concentration_of_nitric_acid_in_air,longitude
  Var_Std_Name: zeit,latitude,height,longitude
  Title: dummy_value
  Var_Name: zeit,lat,z,data_var,lon
```

We want to set `netcdf.Title` and `netcdf.Project` to proper values. We can do this with `slk tag` and take a cup off coffee.

```bash
slk tag /workshop/k204221/mixed_files/test_netcdf_no_attributes_01.nc netcdf.Title="A very special dataset" netcdf.Project="AGP (A great Project)"
Searching for resources in GNS path: /workshop/k204221/mixed_files/test_netcdf_no_attributes_01.nc
Search continuing... ...
Search ID: 205568
[========================================/] 100% complete Metadata applied to 1 of 1 resources. Finishing up... ...

$ slk_helpers metadata /workshop/k204221/mixed_files/test_netcdf_no_attributes_01.nc
netcdf
  Project: AGP (A great Project)
  Var_Long_Name: zeit,latitude,height,mass_concentration_of_nitric_acid_in_air,longitude
  Var_Std_Name: zeit,latitude,height,longitude
  Title: A very special dataset
  Data:
  Var_Name: zeit,lat,z,data_var,lon
netcdf_header
  Var_Long_Name: zeit,latitude,height,mass_concentration_of_nitric_acid_in_air,longitude
  Var_Std_Name: zeit,latitude,height,longitude
  Title: dummy_value
  Var_Name: zeit,lat,z,data_var,lon
```

### Set many metadata fields for many files

```bash
$ slk_helpers metadata /workshop/k204221/mixed_files/test_netcdf_no_attributes_02.nc
netcdf
  Var_Long_Name: zeit,latitude,height,mass_concentration_of_nitric_acid_in_air,longitude
  Var_Std_Name: zeit,latitude,height,longitude
  Title: dummy_value
  Data:
  Var_Name: zeit,lat,z,data_var,lon
netcdf_header
  Var_Long_Name: zeit,latitude,height,mass_concentration_of_nitric_acid_in_air,longitude
  Var_Std_Name: zeit,latitude,height,longitude
  Title: dummy_value
  Var_Name: zeit,lat,z,data_var,lon
```

We have a command `slk_helpers hsm2json` and `slk_helpers json2hsm` which allow extracting metadata from the HSM into JSON files and writing metadata from JSON files to the HSM. Never hear of JSON? This is a JSON file:

```bash
$ cat mixed_files/test_netcdf_no_attributes_0203.json
[
  {
    "path": "/workshop/k204221/mixed_files/test_netcdf_no_attributes_02.nc",
    "tags": {
      "netcdf.Project": "AGP (A great Project)",
      "netcdf.Title": "A very special dataset No 2"
    },
    "provenance": {
      "timeStampISO": "2022-10-10T14:37:02.711201552",
      "software": "own work",
      "formatVersion": "2.0.0",
      "softwareVersion": "none"
    }
  },
  {
    "path": "/workshop/k204221/mixed_files/test_netcdf_no_attributes_03.nc",
    "tags": {
      "netcdf.Project": "AGP (A great Project)",
      "netcdf.Title": "A very special dataset No 3"
    },
    "provenance": {
      "timeStampISO": "2022-10-10T14:37:02.711201552",
      "software": "own work",
      "formatVersion": "2.0.0",
      "softwareVersion": "none"
    }
  }
]
```

We can write metadata for specific files as JSON (in this specific JSON structure). Some explanation:

* everything is stored in a JSON array `[...]`
* one JSON object per file to be modified
* `path`: path of the resource which metadata should be updated
* `tags`: metadata fields to be set

We write these metadata into StrongLink via `slk_helpers json2hsm`:

```bash
$ slk_helpers json2hsm mixed_files/test_netcdf_no_attributes_0203.json

$ slk_helpers metadata /workshop/k204221/mixed_files/test_netcdf_no_attributes_02.nc
netcdf
  Project: AGP (A great Project)
  Var_Long_Name: zeit,latitude,height,mass_concentration_of_nitric_acid_in_air,longitude
  Var_Std_Name: zeit,latitude,height,longitude
  Title: A very special dataset No 2
  Data:
  Var_Name: zeit,lat,z,data_var,lon
netcdf_header
  Var_Long_Name: zeit,latitude,height,mass_concentration_of_nitric_acid_in_air,longitude
  Var_Std_Name: zeit,latitude,height,longitude
  Title: dummy_value
  Var_Name: zeit,lat,z,data_var,lon

$ slk_helpers metadata /workshop/k204221/mixed_files/test_netcdf_no_attributes_03.nc
netcdf
  Project: AGP (A great Project)
  Var_Long_Name: zeit,latitude,height,mass_concentration_of_nitric_acid_in_air,longitude
  Var_Std_Name: zeit,latitude,height,longitude
  Title: A very special dataset No 3
  Data:
  Var_Name: zeit,lat,z,data_var,lon
netcdf_header
  Var_Long_Name: zeit,latitude,height,mass_concentration_of_nitric_acid_in_air,longitude
  Var_Std_Name: zeit,latitude,height,longitude
  Title: dummy_value
  Var_Name: zeit,lat,z,data_var,lon

```

### How to get a JSON template for metadata import?

```bash
$ slk_helpers hsm2json /workshop/k204221/mixed_files/test_netcdf_no_attributes_02.nc
[
  {
    "path": "/workshop/k204221/mixed_files/test_netcdf_no_attributes_02.nc",
    "id": 60996956010,
    "tags": {
      "netcdf.Var_Name": "zeit,lat,z,data_var,lon",
      "netcdf.Project": "AGP (A great Project)",
      "netcdf_header.Title": "dummy_value",
      "netcdf.Var_Std_Name": "zeit,latitude,height,longitude",
      "netcdf.Data": {
        "title": "dummy_value",
        "_NCProperties": "version=2,netcdf=4.8.1,hdf5=1.10.7"
      },
      "netcdf_header.Var_Name": "zeit,lat,z,data_var,lon",
      "netcdf_header.Var_Long_Name": "zeit,latitude,height,mass_concentration_of_nitric_acid_in_air,longitude",
      "netcdf_header.Var_Std_Name": "zeit,latitude,height,longitude",
      "netcdf.Title": "A very special dataset No 2",
      "netcdf.Var_Long_Name": "zeit,latitude,height,mass_concentration_of_nitric_acid_in_air,longitude"
    },
    "provenance": {
      "timeStampISO": "2022-10-08T03:03:35.220347784",
      "software": "slk_helpers",
      "timeStampMillis": 1665191015219,
      "formatVersion": "2.0.0",
      "softwareVersion": "1.5.1"
    }
  }
]


